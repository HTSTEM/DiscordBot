import datetime
import requests
import pickle
import os
import re

from flask import Flask, render_template, send_file, redirect, session, request, abort
from requests_oauthlib import OAuth2Session


config = open('config/discord_oauth.txt').read().strip().split('\n')
OAUTH2_CLIENT_ID = config[0].strip()
OAUTH2_CLIENT_SECRET = config[1].strip()

BASE_API_URL = 'https://discordapp.com/api'
REDIRECT = 'https://yt.htcraft.ml/oauth2'
ME = 'https://yt.htcraft.ml/'

OP_ADD_NEW = 0
OP_SEND_NOTIF = 1

# Voting params
REQUIRED_VOTES = 15
REQUIRED_AGE = 24

# Parsing HTML with RegEx ;)
AVATAR_RE = re.compile(r'<img class="appbar-nav-avatar" src="(.*?)" title="')
TITLE_RE = re.compile(r'<meta name="title" content="(.*?)">')


if True or 'http://' in REDIRECT:
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = 'true'


try:
    with open('state_cache/yt_voting.pkl', 'rb') as file_:
        things_being_voted = pickle.load(file_)
except FileNotFoundError:
    things_being_voted = []


app = Flask(__name__, template_folder='.')
app.config['SESSION_TYPE'] = 'filesystem'
app.secret_key = 'HTBote is best bote'


# Utilities
def get_success(card):
    """Check if a card has finished voting"""
    y, m, n = (*map(lambda x:len(card['votes'][x]), 'ymn'),)
    if y + m + n < REQUIRED_VOTES:
        return None
    if 'added' in card and ((datetime.datetime.now() - card['added']).seconds / 3600) < REQUIRED_AGE:
        return None

    if (((2 * y) - n) / (y + m + n)) < 2/3:
        return None # temporarily disabled
    return None # STOP

def save_things():
    with open('state_cache/yt_voting.pkl', 'wb') as file_:
        pickle.dump(things_being_voted, file_)

def make_session(token=None, state=None, scope=None):
    def token_updater(t):
        session['oauth2_token'] = t

    return OAuth2Session(
        client_id=OAUTH2_CLIENT_ID,
        token=token,
        state=state,
        scope=scope,
        redirect_uri=REDIRECT,
        auto_refresh_kwargs={
            'client_id': OAUTH2_CLIENT_ID,
            'client_secret': OAUTH2_CLIENT_SECRET,
        },
        auto_refresh_url=BASE_API_URL + '/oauth2/token',
        token_updater=token_updater
    )

def get_user():
    # Query Discord for user information
    if 'key' not in session: return
    if 'user' not in session:
        discord = make_session(token=session.get('oauth2_token'))
        session['user'] = discord.get(BASE_API_URL + '/users/@me').json()

    return session['user']

# Routes
@app.route('/oauth2', methods=['GET'])
def oauth2_complete():
    discord = make_session(state=session.get('oauth2_state'))
    session['oauth2_token'] = discord.fetch_token(
        BASE_API_URL + '/oauth2/token',
        client_secret=OAUTH2_CLIENT_SECRET,
        authorization_response=request.url
    )
    session['key'] = request.args.get('code')
    return redirect(ME)

@app.route('/', methods=['GET'])
def index():
    session['guild'] = request.args.get('g')

    if 'key' not in session:
        # Authenticate with Discord
        discord = make_session(scope='identify')
        authorization_url, state = discord.authorization_url(BASE_API_URL + '/oauth2/authorize')
        session['oauth2_state'] = state

        return redirect(authorization_url)

    user = get_user()

    # Serve the page
    return render_template('index.html', things=things_being_voted, user=user)

@app.route('/vote/<chan>/<vote>', methods=['GET'])
def vote(chan, vote):
    user = get_user()
    if user is None:
        return abort(401)

    # Locate the card and update votes
    card = None
    if vote in 'ymn':
        for i in things_being_voted:
            if i['chan'] == chan:
                # Remove all previous votes for the current user
                for j in i['votes']:
                    if user['id'] in i['votes'][j]:
                        i['votes'][j].remove(user['id'])
                # Add our new vote
                i['votes'][vote].append(user['id'])
                card = i

    # Check if the channel is ready to push to the bot
    if card is not None:
        success = get_success(card)

        if success is not None:
            if success:
                # Push to queue ready for bot loop to retreive
                app.mp_queue.put([OP_ADD_NEW, (381941901462470658, chan)])

                msg = f'<https://youtube.com/{chan.replace("=", "/")}> was succesful. Look out for their videos here.'
                app.mp_queue.put([OP_SEND_NOTIF, (480765140820819980, msg)])
            things_being_voted.remove(card)

    save_things()

    return redirect(ME)

@app.route('/add', methods=['GET', 'POST'])
def add():
    user = get_user()
    if user is None:
        # Redirect will force them to login to Discord first
        return redirect('../..')

    if request.method == 'GET':
        return render_template('add.html', user=user)

    chan = request.form.get('url')
    if not chan:
        return abort(400, 'No URL provided')

    try:
        end = chan.split('youtube.com/', 1)[1]
        type, code = end.split('/', 1)
    except:
        # Complain
        return 'URL must be in form:<br><code>https://youtube.com/channel/[code]</code><br>or<br><code>https://youtube.com/user/[user]</code><br><br><a href="/add">Back</a>'

    # Scrape YouTube page for channel name and avatar
    if not chan.startswith('http'):
        chan = 'https://' + chan

    page = requests.get(chan)
    if page.status_code != 200:
        return abort(400, 'Did you provide a valid URL?')

    try:
        title = TITLE_RE.findall(page.text)[0]
        avatar = AVATAR_RE.findall(page.text)[0]
    except IndexError:
        return abort(400, 'Either YouTube was like "No u" or that wasn\'t YouTube.')

    now = datetime.datetime.now()

    for i in things_being_voted:
        if i['chan'] == type + '=' + code:
            break
    else:
        # Push to site with default values
        things_being_voted.append(dict(
            meta=title,
            votes=dict(y=[], m=[], n=[]),
            chan=type + '=' + code,
            image=avatar,
            added=now,
        ))
        save_things()

        msg = f'New channel up for voting at <https://yt.htcraft.ml>! Please vote ASAP! {chan}'
        app.mp_queue.put([OP_SEND_NOTIF, (480765140820819980, msg)])

    return redirect(ME)

@app.route('/lo')
def logout():
    # Clear any OAuth2 keys
    if 'key' in session:
        del session['key']
    return redirect(ME)

@app.route('/index.css')
def style():
    return send_file('index.css')

# Function to start the server with a multiprocessing binding
def yt_site(queue, port=4567, host='0.0.0.0'):
    # Bind our event queu to the flask server
    app.mp_queue = queue
    # Start flask properly
    # If debug=True is set, werkzeug will attempt to live-reload **all** code
    # including things like d.py cogs. This is bad.
    app.run(debug=False, port=port, host=host)
